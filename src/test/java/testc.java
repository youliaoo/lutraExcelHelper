/**
 * @author pcode@qq.com
 * @copyright youliaoo.com
 * @createtime 2018/6/15
 */
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.youliaoo.utils.excelOpterator;
import org.junit.jupiter.api.Test;

public class testc {
    @Test
    public void test1() throws Exception {
        excelOpterator eo = new excelOpterator().setHeadRowIndex(0);
        JSONObject keys = eo.genImportKyes(new String[]{"序号", "数量", "日期", "小计", "备注"},new String[]{"id","count","date","sum","memo"} );
        //导入excel
        JSONArray rows = eo.readXls("C:/excel.xls", keys);
        //转换成pojo实例
        for(Object jo :rows){
            testEntity to = JSON.toJavaObject((JSONObject)jo,testEntity.class );
            System.out.println(to);
        }
        //导出excel
        eo.writeXls(rows, "C:/1.xlsx",keys );
    }
}
