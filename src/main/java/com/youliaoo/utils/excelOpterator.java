package com.youliaoo.utils;
/**
 * 通用的excel解析和导入导出工具
 * 准备使用json作为中间数据转换使用。
 * excel->json->class这个套路来试试看，这样就不用担心excel的列的位置问题了。但是还有一个转换的问题，还需要解决一次
 * 反过来的时候class->json->class。
 * 不过层次不能太深，不然就套在里面了。
 *
 * @author pcode@qq.com
 * @copyright youliaoo.com
 * @createtime 2018/6/15
 */

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class excelOpterator {
    private int headRowIndex;
    private final static String XLS = "xls";//2003
    private final static String XLSX = "xlsx";//2003+
    /**
     * 设置一下读数据头的哪一行的位置，所有的数据都从这一行之后开始读取
     * @param headRowIndex
     * @return
     */
    public excelOpterator setHeadRowIndex(int headRowIndex) {
        this.headRowIndex = headRowIndex;
        return this;
    }

    /**
     *
     * @param cell
     * @return
     */
    private String parseCell(Cell cell) {
        //这句用于处理特别特殊的日期格式如果发现特殊不能解析的情况请查看这个输出的值并修改对应的处理函数
        //System.out.println("dataformat:"+cell.getCellStyle().getDataFormat());
        String result;
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:// 数字类型
//                System.out.println("dataformat:"+cell.getCellStyle().getDataFormat());
                if (DateUtil.isCellDateFormatted(cell)) {// 处理日期格式、时间格式
                    SimpleDateFormat sdf = null;
                    if (cell.getCellStyle().getDataFormat() == HSSFDataFormat
                            .getBuiltinFormat("h:mm")) {
                        sdf = new SimpleDateFormat("HH:mm");
                    } else {// 日期
                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                    }
                    Date date = cell.getDateCellValue();
                    result = sdf.format(date);
                } else if (cell.getCellStyle().getDataFormat() == 176 || cell.getCellStyle().getDataFormat() == 179) { //这里需要额外判断一下格式的id
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
                    double value = cell.getNumericCellValue();
                    Date date = org.apache.poi.ss.usermodel.DateUtil
                            .getJavaDate(value);
                    result = sdf.format(date);
                } else if (cell.getCellStyle().getDataFormat() == 58) {
                    // 处理自定义日期格式：m月d日(通过判断单元格的格式id解决，id的值是58)
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    double value = cell.getNumericCellValue();
                    Date date = org.apache.poi.ss.usermodel.DateUtil
                            .getJavaDate(value);
                    result = sdf.format(date);
                } else {
                    double value = cell.getNumericCellValue();
                    CellStyle style = cell.getCellStyle();
                    DecimalFormat format = new DecimalFormat();
                    String temp = style.getDataFormatString();
                    // 单元格设置成常规
                    if (temp.equals("General")) {
                        format.applyPattern("#");
                    }
                    result = format.format(value);
                }
                break;
            case Cell.CELL_TYPE_STRING:// String类型
                result = cell.getRichStringCellValue().toString();
                break;
            case Cell.CELL_TYPE_BLANK:
                result = "";
                break;
            case Cell.CELL_TYPE_FORMULA:
                try {
                    result = String.valueOf(cell.getStringCellValue());
                } catch (IllegalStateException e) {
                    result = String.valueOf(cell.getNumericCellValue());
                }
                break;
            default:

                result = "";
                break;
        }
        return result;
    }

    /**
     * 使用第一行来建立字典，也可以不建立字典直接使用第一行
     * @param row
     * @param keys
     * @return
     */
    private JSONObject createColMapper(Row row, JSONObject keys) {
        int col_count = row.getLastCellNum();
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < col_count; i++) {
            String cv = row.getCell(i).getStringCellValue();
            if (keys == null) {
                jsonObject.put(cv, i);
            } else {
                String key = keys.getString(cv);
                jsonObject.put(key, i);
            }
        }
        return jsonObject;
    }

    /**
     * 根据给定的规则来读取这一行数据
     *
     * @param row     sheet中的一行
     * @param rowkeys rowkey是预先定义好的key在row中的位置的一个json 比如{"id":0,"count":1....}
     * @return
     */
    private JSONObject getJsonRow(Row row, JSONObject rowkeys) {
        JSONObject result = new JSONObject();
        for (String key : rowkeys.keySet()) {
            int col_index = rowkeys.getInteger(key);//获取到字典里定义的列明与列位置的对照关系
            Cell cell = row.getCell(col_index);//读取列的值
            Object cv = parseCell(cell);//解析单元格
            result.put(key, cv);//放到结果里面
        }
        return result;
    }

    private Workbook createWorkBook(File file){
        Workbook workbook = null;
        String extensionName = FilenameUtils.getExtension(file.getName());
        if (extensionName.toLowerCase().equals(XLS)) {
            workbook = new HSSFWorkbook();
        } else if (extensionName.toLowerCase().equals(XLSX)) {
            workbook = new XSSFWorkbook();
        }
        return workbook;
    }
    private Workbook getWorkBook(File file) {
        Workbook workbook = null;
        try {
            String extensionName = FilenameUtils.getExtension(file.getName());
            InputStream is = new FileInputStream(file);
            if (extensionName.toLowerCase().equals(XLS)) {
                workbook = new HSSFWorkbook(is);
            } else if (extensionName.toLowerCase().equals(XLSX)) {
                workbook = new XSSFWorkbook(is);
            }
        }catch (FileNotFoundException e1){
            e1.printStackTrace();
        }catch (IOException e2){
            e2.printStackTrace();
        }
        return workbook;

    }

    /**
     * 读取xls文件到json数组
     * @param path
     * @param keys
     * @return
     */
    public JSONArray readXls(String path, JSONObject keys) {
        JSONArray result = new JSONArray();
        try {
            File inputfile = new File(path);
            Workbook workBook = getWorkBook(inputfile);
            Sheet sheet0 = workBook.getSheetAt(0);//获取第一个sheet
            JSONObject colMapper = createColMapper(sheet0.getRow(headRowIndex), keys);//使用指定行做转译json属性的key字典
            for (int row_index = headRowIndex + 1; row_index < sheet0.getLastRowNum(); row_index++) {//从头部行以后读取所有行
                JSONObject jo = getJsonRow(sheet0.getRow(row_index), colMapper);//解析一行
                result.add(jo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 创建用于翻译中文列名到英文变量名的对照字典。
     * @param keys  中文或默认的excel列名称
     * @param values 变量名
     * @return
     * @throws Exception
     */
    public JSONObject genImportKyes(String[] keys, String[] values) throws Exception {
        try {
            JSONObject result = new JSONObject();
            for (int i = 0; i < keys.length; i++) {
                result.put(keys[i], values[i]);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * 输出头行，并将头行对应的字段位置返回
     * @param sheet
     * @param keys
     * @return
     */
    private JSONObject createColMapper(Sheet sheet, JSONObject keys){
        JSONObject result =new JSONObject();
        Row headrow = sheet.createRow(headRowIndex);
        int col=0;
        for(String key:keys.keySet()){                              //循环keys
            Cell cell=headrow.createCell(col,Cell.CELL_TYPE_STRING);//创建cell
            cell.setCellValue(key);                                 //写key的值，也就是中文字段值
            result.put(keys.getString(key), col);                   //写属性值做key，写col列位置作为value
            col+=1;                                                 //增长列index
        }
        return result;
    }
    /**
     * 根据colmapper来写出具体的数据
     * @param sheet
     * @param datas
     * @param colmapper
     */
    private void writeBody(Sheet sheet,JSONArray datas,JSONObject colmapper){
        int rowindex= headRowIndex+1;
        for(Object jo :datas){
            JSONObject jsonObject=(JSONObject)jo;           //将一个对象转换成json
            Row newrow = sheet.createRow(rowindex);         //为它创建一个行
            //写一行数据
            for(String key:colmapper.keySet()){             //循环它的所有列
                int colindex = colmapper.getInteger(key);   //从计算好的colmapper里面位置
                Cell newcell = newrow.createCell(colindex,Cell.CELL_TYPE_STRING);//创建单元格
                newcell.setCellValue(jsonObject.getString(key));//设置单元格
            }
            rowindex +=1;
        }
    };
    /**
     * 写json数组到xls文件
     * @param datas
     * @param path
     * @param keys
     */
    public void writeXls(JSONArray datas ,String path,JSONObject keys){
        //创建文件
        File file = new File(path);
        //创建工作簿
        Workbook workBook = createWorkBook(file);
        //创建工作表
        Sheet sheet = workBook.createSheet("数据");
        //创建行
        JSONObject colmapper = createColMapper(sheet,keys );
        writeBody(sheet,datas,colmapper);
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            workBook.write(outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
