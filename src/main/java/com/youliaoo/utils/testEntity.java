package com.youliaoo.utils;/**
 * @author pcode@qq.com
 * @copyright youliaoo.com
 * @createtime 2018/6/15
 */

import java.sql.Time;
import java.sql.Timestamp;

/**
 testEntity 类
 @author
 @version Copy Right by youliaoo.com.2011~2018
 **/
public class testEntity {
    private int id;
    private double count;
    private Timestamp date;
    private String memo;
    private double sum;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("testEntity{");
        sb.append("id=").append(id);
        sb.append(", count=").append(count);
        sb.append(", date='").append(date).append('\'');
        sb.append(", memo='").append(memo).append('\'');
        sb.append(", sum=").append(sum);
        sb.append('}');
        return sb.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
