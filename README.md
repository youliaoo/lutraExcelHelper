# lutraExcelHelper

#### 项目介绍
一、这个项目主要完成以下两件事情：
1.从excel里读取行集数据，把它变成json对象；
2.把json数组输出到excel；

二、这么简单的事情还是有可以做好的空间：
1.字段表达式中文的，但是类的属性是英文的，json的属性也是英文的，导入导出的时候如何对应？
2.代码的空间里，属性其实是没有顺序的，但是excel的列是有顺序的，这如何建立联系，让列的位置与类的属性直接建立联系，而不是通过固定的模版位置来确定？

三、这个项目就是破解以上两个问题做了一些常识，用json作为POJO和EXCEL的中间传递介质，以ORM的思路来实现EXCEL的导入导出。开起实现的还比较优雅。因为代码很短。


#### 软件架构
使用POI读取和写入excel，第一步建立中文字段名与属性的对应关系，第二步通过中文字段名与excel列名的建立属性与列名的对照关系，第三步按行读取excel根据数据类型写json，第四步，将json转换成我们需要的类的实例。


#### 使用说明
```
/**
 * @author pcode@qq.com
 * @copyright youliaoo.com
 * @createtime 2018/6/15
 */
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.youliaoo.utils.excelOpterator;
import org.junit.jupiter.api.Test;

public class testc {
    @Test
    public void test1() throws Exception {
        //指定读取首行的位置，也就是列名的行号从0开始。数据的读取从这行之后开始。
        excelOpterator eo = new excelOpterator().setHeadRowIndex(0);
        JSONObject keys = eo.genImportKyes(new String[]{"序号", "数量", "日期", "小计", "备注"},new String[]{"id","count","date","sum","memo"} );
        //导入excel
        JSONArray rows = eo.readXls("C:/excel.xls", keys);
        //转换成pojo实例
        for(Object jo :rows){
            testEntity to = JSON.toJavaObject((JSONObject)jo,testEntity.class );
            System.out.println(to);
        }
        //导出excel
        eo.writeXls(rows, "C:/1.xlsx",keys );
    }
}
```